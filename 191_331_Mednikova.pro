QT += quick
QT += network

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        cryptocontroller.cpp \
        main.cpp \
        vkmodel.cpp \
        webappcontroller.cpp

RESOURCES += qml.qrc

INCLUDEPATH += \
        C:/mySSL_Lab6/include/openssl # загаловочные файлы
win32 {
    LIBS += \
            C:/mySSL_Lab6/lib/libcrypto.lib # библиотека, содержащая таблицу соответствий названий функций и их адресов

    LIBS += -L"C:/mySSL_Lab6"
}
android {
    LIBS += \
        C:/Users/1353210/AppData/Local/Android/Sdk/android_openssl/static/lib/arm64/libssl.a \
        C:/Users/1353210/AppData/Local/Android/Sdk/android_openssl/static/lib/arm64/libcrypto.a
}
INCLUDEPATH += C:/mySSL_Lab6/include

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

#### БИБЛИОТЕКИ ДЛЯ FIREBASE
##Зоя, ctrl + / на eng раскладке

#INCLUDEPATH += C:/firebase_cpp_sdk/include

#INCLUDEPATH += \
#        C:/mySSL_Lab6/include/openssl # загаловочные файлы

#win32{
#    LIBS += \
#            C:/mySSL_Lab6/lib/libcrypto.lib # библиотека, содержащая таблицу соответствий названий функций и их адресов

#    LIBS += -L"C:/mySSL_Lab6"
#}

#android {
#    LIBS += \
#        C:/Users/1353210/AppData/Local/Android/Sdk/android_openssl/static/lib/arm64/libssl.a \
#        C:/Users/1353210/AppData/Local/Android/Sdk/android_openssl/static/lib/arm64/libcrypto.a
#}

#INCLUDEPATH += C:/mySSL_Lab6/include

#android {
#    QT += androidextras
#    LIBS += \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_admob.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_analytics.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_app.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_auth.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_database.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_dynamic_links.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_firestore.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_functions.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_installations.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_instance_id.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_messaging.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_remote_config.a \
#        C:/firebase_cpp_sdk/libs/android/arm64-v8a/c++/libfirebase_storage.a
#}

#win32 {
#    LIBS += \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_admob.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_analytics.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_app.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_auth.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_database.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_dynamic_links.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_firestore.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_functions.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_installations.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_instance_id.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_messaging.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_remote_config.lib \
#        C:/firebase_cpp_sdk/libs/windows/VS2019/MD/x64/Debug/firebase_storage.lib
#}
#### БИБЛИОТЕКИ ДЛЯ FIREBASE

RC_ICONS = "images/icon.ico"

HEADERS += \
    cryptocontroller.h \
    vkmodel.h \
    webappcontroller.h
android: include(C:\Users\1353210\AppData\Local\Android\Sdk\android_openssl/openssl.pri)
