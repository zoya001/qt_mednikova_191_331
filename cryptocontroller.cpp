#include "cryptocontroller.h"
#include <QString>
#include "openssl/evp.h"
#include <QFile>
#include <QByteArray>
#include <QIODevice>
#include <QObject>
#include <QTemporaryFile>
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/aes.h>
#include <QBuffer>
#include <QDebug>

Encryptioncontroller::Encryptioncontroller(QObject *parent):QObject (parent){}

bool Encryptioncontroller::encryptFile(const QString & mkey, const QString & in_file){
    EVP_CIPHER_CTX *ctx;
    if(!(ctx = EVP_CIPHER_CTX_new())) {
        return false;
    }
    if (mkey.length() == 32) {
        key = (unsigned char*) mkey.constData();
        if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cfb(), NULL, reinterpret_cast<unsigned char *>(mkey.toUtf8().data()), key))
        {
            return false;
        }
    }
    else {
        key2 = "3210423012345889873884547892764";
        //шифруем по aes 256, преобразуем из unsigned char в toUtf8 и ключ
        if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cfb(), NULL, reinterpret_cast<unsigned char *>(key2.toUtf8().data()), key))
        {
            return false;
        }
    }

    unsigned char ciphertext[2048] = {0};
    unsigned char plaintexttext[2048] = {0};
    int len = 0, plaintext_len = 0;

    soursefile = in_file.mid(8); //отделяем файл
    //qDebug() << "+++++++++++++++++++" << in_file;
    QFile sourse_file(soursefile);
    sourse_file.open(QIODevice::ReadWrite);

    int position = soursefile.lastIndexOf(".");
    QString file_extension = soursefile.mid(position);
    QString soursefile_enc = soursefile.left(position) + "_encryption" + file_extension;


    QFile file_modificate(soursefile_enc);
    file_modificate.open(QIODevice::ReadWrite | QIODevice::Truncate);
    plaintext_len = sourse_file.read((char *)plaintexttext, 2048);
    // пока plaintext_len не равен нулю, шифруем символы
    // по кускам шифрование
    while(plaintext_len > 0) {
        if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintexttext, plaintext_len))
        {
            return false;
        }
        file_modificate.write((char *)ciphertext, len);
        plaintext_len = sourse_file.read((char *)plaintexttext, 2048);
    }

    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
    {
        return false;
    }

    file_modificate.write((char*)ciphertext, len);
    EVP_CIPHER_CTX_free(ctx);

    sourse_file.close();
    file_modificate.close();
    return true;
}

bool Encryptioncontroller::decryptFile(const QString & mkey, const QString & in_file){
    EVP_CIPHER_CTX *ctx;
    if(!(ctx = EVP_CIPHER_CTX_new())) {
        return false;
    }
    if (mkey.length() == 32){
        key = (unsigned char*) mkey.data();

        if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cfb(), NULL, reinterpret_cast<unsigned char *>(mkey.toUtf8().data()), key)) {
            return false;
        }
    }
    else {
        if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cfb(), NULL, reinterpret_cast<unsigned char *>(key2.toUtf8().data()), key)) {
            return false;
        }
    }

    unsigned char ciphertext[2048] = {0};
    unsigned char plaintexttext[2048] = {0};
    int len = 0, plaintext_len = 0;
    soursefile = in_file.mid(8);
    QFile sourse_file(soursefile);
    sourse_file.open(QIODevice::ReadWrite);

    plaintext_len = sourse_file.read((char *)plaintexttext, 2048);
    QString cipher;
    while(plaintext_len > 0) {
        if(1 != EVP_DecryptUpdate(ctx, ciphertext, &len, plaintexttext, plaintext_len))
        {
            return false;
        }
        plaintext_len = sourse_file.read((char *)plaintexttext, 2048);
        cipher = cipher +  QString::fromUtf8((char*)(ciphertext));
    }
    qDebug() << cipher;
    emit sendChiprToQml(cipher);

    if(!EVP_DecryptFinal_ex(ctx, ciphertext + len, &len))
                return false;
    EVP_CIPHER_CTX_free(ctx);
    sourse_file.close();

    return true;
}
