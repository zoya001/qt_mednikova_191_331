import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Styles 1.4


Page {
    id:page3
    header: Rectangle {
        id:page3_header
        color: "#76b947"
        height: 32

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page3_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page3_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            text: qsTr("ЛР3. Запросы к серверу.")
            font.pixelSize : 18
            anchors.verticalCenter: page3_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page3_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page3_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page3_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    Universal.theme: theme.position < 1 ? Universal.Dark : Universal.Light

    ColumnLayout {
           anchors.margins: 10
           anchors.fill: parent

           RowLayout {
               Layout.alignment: Qt.AlignHCenter
               Label {
                   text: "Темная тема"
               }
               Switch {
                   id: theme
                   checked: false
                   Universal.accent: "#76b947"
               }
               Label {
                   text: "Светлая тема"
               }
           }

           RowLayout {
               Button {
                   id: request_button
                   Layout.alignment: Qt.AlignCenter
                   onClicked: mainWindow.btnHttpRequest();
                   text: "Отправить запрос"
                   Universal.accent: "#76b947"
                   Layout.fillWidth: true
               }
           }

           RowLayout {
               id: change
               Layout.alignment: Qt.AlignCenter

               RadioButton {
                   checked: true
                   text:"Rich Text"
                   Universal.accent: "#76b947"
                   onClicked: response.textFormat = Text.RichText
               }

               RadioButton {
                   text: "Plain Text"
                   Universal.accent: "#76b947"
                   onClicked: response.textFormat = Text.PlainText
               }
           }

           ColumnLayout {
               id: response_body

               Rectangle {
                   anchors.fill: response_body
                   color: "#76b947"
               }

               ScrollView {
                   clip: true
                   Layout.fillWidth: true
                   Layout.fillHeight: true

                   Text{
                       id: response
                       anchors.fill: parent
                       objectName: "textArea"
                       textFormat: Text.RichText
                   }
               }
           }
               Layout.alignment: Qt.AlignCenter

               RowLayout {
                   Layout.alignment: Qt.AlignCenter
                   Layout.fillWidth: true

                   Label {
                       text: qsTr("Текущая температура gismeteo.ru:")
                   }

                   TextField {
                       id: textField
                       objectName: "textField"
                       Universal.accent: "#76b947"
                       readOnly: true //иначе можно влезть в форму
                       Layout.fillWidth: true
                       horizontalAlignment: Text.AlignHCenter
                   }
               }
           }
    }

