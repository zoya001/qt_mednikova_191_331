#include <QNetworkRequest>
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QtWidgets/QTableView>
#include "webappcontroller.h"
#include "vkmodel.h"

WebAppController::WebAppController(QObject *parent) : showInfo(parent)
{
    nam = new QNetworkAccessManager(this); //создаем NetworkAccessManager
    connect(nam, &QNetworkAccessManager::finished, this, &WebAppController::onNetworkValue);
    vk_model = new vkModel();
}

void WebAppController::getPageInfo(){
    QEventLoop evtLoop;
    connect(nam, &QNetworkAccessManager::finished,
            &evtLoop, &QEventLoop::quit);
    nam->get(QNetworkRequest(QUrl("https://www.gismeteo.ru"))); // получаем ответ
    evtLoop.exec();

}

void WebAppController::onNetworkValue(QNetworkReply *reply)
{ // парсинг сайта

    QString str = reply->readAll(); // записываем в str страницу https://www.gismeteo.ru

    QObject* textArea = showInfo->findChild<QObject*>("textArea"); //ищем элемент textArea в qml для str
    QObject* textField = showInfo->findChild<QObject*>("textField"); //ищем элемент textField в qml для аутпута
    textArea->setProperty("text", str); //записываем в параметр текст строку, в которой вся страница
    int i = str.indexOf("unit unit_temperature_c"); // ищем первое вхождение подстроки
    textField->setProperty("text", str.mid(i + 78,3)  + "°"); // получаем температуру, делаем пропуск и выводим
                                                       // все нормально
    qDebug() << "ОТВЕТ ОТ ГИСМЕТИО" << reply;
}

QString WebAppController::showToken (QString add){ // функия для вывода access_token
    qDebug() <<"Вывод успеха:"<< add;
    if (add.indexOf("access_token=") != -1)
    {
        m_accessToken = add.split("access_token=")[1].split("&")[0]; //запись в переменную
        qDebug() << "ТОКЕН: " << m_accessToken;
        return m_accessToken;
    }
    else {
        qDebug() << "Подождем"; // возвращаем пустую строку на случай ошибки
    }
    QString str = " ";
    return str;
}

void WebAppController::restRequest() { //коннект
    QEventLoop loop;
    nam = new QNetworkAccessManager();

    //получаем данные о юзерах
    QObject::connect(nam, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));
    QNetworkReply *reply = nam->get(QNetworkRequest(QUrl( "https://api.vk.com/method/friends.get?v=5.52&fields=photo_100,city,status,domain,sex"
                                                          "&access_token="+ m_accessToken)));
    //get по аксес-токену
    loop.exec();

    QJsonDocument document = QJsonDocument::fromJson(reply->readAll()); //возвращаем массив json-элементов
    QJsonObject root = document.object();
    QJsonValue result = root.value("response");
    qDebug() << result;
    QJsonObject smth = result.toObject();
    QJsonArray array = smth["items"].toArray();
    qDebug() <<"friend: "<< array.count();

    //парсинг
    for(int i = 0; i < array.count(); i++){
        QJsonObject value = array.at(i).toObject();
        QString name = value.value("first_name").toString();
        QString surname = value.value("last_name").toString();
        QUrl photo = value.value("photo_100").toString();
        QString city;
        QJsonValue Thiscity = value.value("city");
        if (Thiscity != NULL) //ПРОВЕРКА, что поле не пустое
        {
        QJsonObject ThiscityObject = Thiscity.toObject();
            if (ThiscityObject.empty() != true)
            {
              city = ThiscityObject["title"].toString();
            }
        }
        QString domain = value.value("domain").toString();
        vk_model->addingNewItem(vkObject (photo, name, surname, city, domain)); //в метод записываем значения
        //получаем массив json, получили нужные нам значения, записали в класс
    }
}

void WebAppController::getPhoto(){
   QUrl myPhoto;

   QEventLoop loop;
   nam = new QNetworkAccessManager();

   QObject::connect(nam, // связываем loop  с нашим менеджером
                    SIGNAL(finished(QNetworkReply*)),
                    &loop,
                    SLOT(quit()));

   QNetworkReply *reply = nam->get(QNetworkRequest(QUrl( "https://api.vk.com/method/users.get?uids=qaatdb&fields=photo_200,status&v=5.130"
                                                       "&access_token="+ m_accessToken)));

   loop.exec();

   QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
   QJsonObject root = document.object();
   QJsonValue itog = root.value("response");
   QJsonArray array = itog.toArray();
   qDebug() <<"Аватар: "<< array;

   for(int i = 0; i < array.count(); i++){
       QJsonObject value = array.at(i).toObject();

       QUrl photo = value.value("photo_200").toString();
       emit sendToQml(photo);
   }
}

void WebAppController::logIN(QString login, QString password) {
   QNetworkRequest request(QUrl("http://laba9.std-1219.ist.mospolytech.ru/api/auth/login"));
   QByteArray body = "email=mednikova@gmail.com&password=123456";
   request.setRawHeader("Content-Type","application/x-www-form-urlencoded");
   request.setRawHeader("Accept","application/json");

   if (login == "mednikova@gmail.com" && password == "123456"){
       QEventLoop loop;

       nam = new QNetworkAccessManager();

       QObject::connect(nam, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));

       QNetworkReply *reply = nam->post(request, body);
       loop.exec();

       auto reply_body = reply->readAll();
       qDebug() << "ОТВЕТ" << reply_body; //вывод ответа

       QJsonDocument document = QJsonDocument::fromJson(reply_body);
       QJsonObject root = document.object();
       QJsonValue itog = root.value("access_token");
       QString token = itog.toString();
       JWT_token = token;
       qDebug() << "TOKEN" << JWT_token;
       emit tokenToQml(token);
   }
   getData();
   JWT_token = "";
}

void WebAppController::getData() {
   if (JWT_token.length() > 0) {
       QNetworkRequest request;
       request.setUrl(QUrl("http://laba9.std-1219.ist.mospolytech.ru/JwtAPI"));
       QNetworkReply * reply;
       QEventLoop evntLoop;
       connect(nam, &QNetworkAccessManager::finished, &evntLoop, &QEventLoop::quit);
       reply = nam->get(request);
       evntLoop.exec();
       QString replyString = reply->readAll();
       qDebug() << "Данные" << replyString;
       dataToQml(replyString);
   }
   else {
       QString replyString = "<h1>Доступ запрещен</h1>";
       dataToQml(replyString);
   }
}
