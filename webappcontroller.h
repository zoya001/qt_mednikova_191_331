#ifndef WEBAPPCONTROLLER_H
#define WEBAPPCONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include <QCryptographicHash>

#include "vkmodel.h"

class WebAppController : public QObject
{
    Q_OBJECT
public:
    explicit WebAppController(QObject *parent = nullptr); //конструктор класса

    QNetworkAccessManager * nam = nullptr;

    QString m_accessToken;
    vkModel *vk_model;
    QString session_secr;
    QString JWT_token;

    ~WebAppController() { //деструктор класса
        delete nam;
    }

signals:
    void sendToQml(QUrl url);
    void tokenToQml(QString token);
    void dataToQml(QString pageContent);

public slots:
    void getPageInfo(); //слот для составления http-запроса и его отправки к серверу
    void onNetworkValue(QNetworkReply *reply); //слот для приема возвращенной сервером информации
    QString showToken (QString add); //вывод токена
    void restRequest();
    void getPhoto();
    void logIN(QString login, QString password);
    void getData();

protected:
    QObject * showInfo; // создание объекта QObject для нахождения нужных компонентов в qml
};

#endif // WEBAPPCONTROLLER_H
