import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtMultimedia 5.15
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.15

Page {
    id: page8
    header: Rectangle {
        id:page8_header
        color: "#76b947"
        height: 32

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page8_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page8_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            id: chatTest
            text: qsTr("ЛР7. Чат-сервер. ")
            font.pixelSize : 20
            anchors.verticalCenter: page8_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Label {
            font.pixelSize : 20
            anchors.left: chatTest.right
            text: socket_status.text
            anchors.verticalCenter: page8_header.verticalCenter
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page8_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page8_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page8_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    TextInput { id: socket_status; }

    ListModel {
        id: list
    }

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: enterPanel.top
        width: parent.width
        height: parent.height*0.85

        Image {
            Layout.alignment: Qt.AlignCenter
            source: "images/back3.jpg"
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.left: parent.left

            ListView {
            anchors.fill: parent
            anchors.bottomMargin: 70
            anchors.topMargin: 10
            spacing: 4
            model: list
            delegate:
                Item {
                width: parent.width
                height: messageImage.height
                //компонент для появления сообщений
                BorderImage {
                    id: messageImage
                    source: sendMessage ? "images/back2.png" : "images/back1.png" // сами сообщения
                    width: parent.width *0.8
                    height: textMessage.contentHeight + 25
                    anchors.left: sendMessage ? undefined : parent.left
                    anchors.right: sendMessage ? parent.right : undefined

                    Text {
                        id: textMessage
                        color: "black"
                        text: message
                        width: messageImage.width*0.8
                        font.pointSize: 12
                        anchors.top: messageImage.top
                        anchors.left: messageImage.left
                        anchors.leftMargin: 15
                        anchors.topMargin: 6
                        wrapMode: TextArea.WrapAtWordBoundaryOrAnywhere
                    }

                    Text {
                        color: "black"
                        text: Qt.formatDateTime(new Date(), "hh:mm — dd.MM.yyyy")
                        font.pointSize: 8
                        anchors.bottom: messageImage.bottom
                        anchors.right: messageImage.right
                        anchors.rightMargin: 15
                        anchors.bottomMargin: 5
                    }
                }
            }
        }
    }
}

    Rectangle {
            id: enterPanel
            color: "#3d3d3d"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            width: parent.width
            height: parent.height*0.1

            RowLayout {
                anchors.bottom: parent.bottom
                width: parent.width

                Rectangle {
                    anchors.fill: parent
                    color: "#1e1e1e"
                }

                TextArea {
                    id: textToSend
                    Layout.fillWidth: true
                    Layout.margins: 5
                    font.pointSize: 12
                    color: "white"
                    placeholderTextColor: "white"
                    placeholderText: "Введите текст сообщения"
                    wrapMode: TextArea.WrapAtWordBoundaryOrAnywhere
                    background:
                        Item {
                        width: parent.width
                        height: parent.height
                    }
                }

                RoundButton {
                    Universal.background: "#000000"
                    text: "🖂"
                    Layout.margins: 5
                    onClicked: {
                        if(textToSend.text!="") {
                            list.append({
                                 "sendMessage": true,
                                 "message": textToSend.text,
                                 "date" : new Date().toLocaleString(Qt.locale("ru_RU"))
                            });
                            websocket.sendTextMessage(textToSend.text);
                            textToSend.clear();
                        }
                    }
                }
            }
        }


    WebSocket {
        id: websocket
        active: true
//        url: "ws://192.168.137.1:8000"
        url: "ws://websocket-zoe4ka.herokuapp.com"
        onTextMessageReceived: {
            console.log("message: ", message);
            list.append(
                        {
                            "sendMessage" : false,
                            "message" : message,
                            "date" : new Date().toLocaleString(Qt.locale("ru_RU"))
                        });
        }
        onStatusChanged: {
            switch(status)
            {
            case WebSocket.Connecting:
                socket_status.text = "Идет подключение"
                console.log("Socket is connecting");
                break;
            case WebSocket.Open:
                console.log("Socket is open");
                socket_status.text = "Сокет открыт"
                break;
            case WebSocket.Closing:
                socket_status.text = "Сокет закрывается"
                console.log("Socket is closing");
                break;
            case WebSocket.Closed:
                console.log("Socket is closed");
                socket_status.text = "Сокет закрыт"
                break;
            case WebSocket.Error:
                socket_status.text = "Ошибка сокета!"
                console.log("Socket is error");
                console.log("Error = ", errorString);
                break;
            }
        }
    }
}
