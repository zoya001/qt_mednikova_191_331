import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtMultimedia 5.15
import QtQuick.Dialogs 1.0

Page {
    id: page6
    header: Rectangle {
        id:page6_header
        color: "#76b947"
        height: 32

        Connections {
            target: crypt // Указываем целевой объект для соединения
            onSendChiprToQml: {
                cryptoarea.text = codetext;
            }
        }

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page6_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page6_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            text: qsTr("ЛР6. Шифрование данных.")
            font.pixelSize : 18
            anchors.verticalCenter: page6_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page6_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page6_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page6_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.topMargin: 5
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        anchors.bottomMargin: 20
        Layout.alignment: Qt.AlignHCenter
        RowLayout {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            ColumnLayout {
                Button {
                    id: cryptBtn
                    text: "Зашифровать"
                    flat: true
                    onClicked: {crypt.encryptFile(key.text, loadFile.fileUrls)
                        if (crypt.encryptFile(key.text, loadFile.fileUrls) === true){
                        }
                    }
                    font.pixelSize : 18
               }
       }

            ColumnLayout {
                anchors.rightMargin: 20
                anchors.leftMargin: 20
                Button {
                    flat: true
                    text: "Выбрать файл"
                    onClicked: loadFile.open()
                    FileDialog {
                        id: loadFile
                        folder: "C://"
                    }
                    font.pixelSize : 18
                }
            }

            ColumnLayout {
                Button {
                    id: decrypt
                    text: "Расшифровать"
                    flat: true
                    onClicked: {crypt.decryptFile(key.text, loadFile.fileUrls)
                        if (crypt.decryptFile(key.text, loadFile.fileUrls) === true){}
                    }
                    font.pixelSize : 18
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            TextField {
                maximumLength: 32
                id: key
                placeholderText: qsTr("Введите ключ")
                placeholderTextColor: "#76b947"
                Layout.fillWidth: true
                Layout.fillHeight: true
                font.pixelSize : 18
                Universal.accent: "#76b947"
            }
        }

        RowLayout {
            ScrollView {
            id:scroll
            TextArea {
                id: cryptoarea
                textFormat: Text.RichText
                overwriteMode: true
                readOnly: true
                wrapMode: TextArea.Wrap
                font.pixelSize : 18
                Universal.accent: "#76b947"
            }
            Layout.fillWidth: true
            Layout.fillHeight: true
            }
        }
    }
}
