import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtMultimedia 5.15
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.15

ApplicationWindow {
    id:mainWindow
    width: 500
    height: 600
    visible: true
    title: qsTr("MobDev")

    signal btnHttpRequest()
    signal signalMakeRequest();
    signal showToken (string add); // объявление сигнала
    signal restRequest();
    signal getPhoto();
    signal btnHTTPResponse()

    SwipeView {
        id: swipeView
        anchors.fill: parent

        Page1 {
        }

        Page2 {
            id: page2
        }

        Page3 {
            id: page3
        }

        Page4 {
            id: page4
        }

        Page5 {
            id: page5
        }

        Page6 {
            id: page6
        }

        Page7 {
            id: page7
        }

        Page8 {
            id: page8
        }

        Page9 {
            id: page9
        }
    }

    Connections {
        target: httpController
        onSendToQml: {
            avatar.source = url // Устанавливаем аву
        }
    }

    Drawer {
        id: drawer
        width: 0.6 * parent.width
        height: parent.height

        GridLayout {
        width: parent.width
        columns: 1
        Item {
            Image {
                id: avatar
                width: 46
                height: 46
                Layout.alignment: Qt.AlignCenter
                layer.enabled: true
                layer.effect: OpacityMask {
                    maskSource: mask2
                    anchors.fill: avatar
                }
            }
            Rectangle {
                id: mask2
                width: 46
                height: 46
                radius: 24
                visible: false
                }
        }

        Label {
            anchors.left: avatar.right
            id: navigation
            text: "Навигация"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 25
            color: "#76b947"
        }

            Button {
                text: "ЛР1. Элементы GUI."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 0
                drawer.close()
                }
            }

            Button {
                text: "ЛР2. Работа с фото и видео."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 1
                drawer.close()
                }
            }

            Button {
                text: "ЛР3. Запросы к серверу."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 2
                drawer.close()
                }
            }

            Button {
                text: "ЛР4. Аутентификация OAuth2."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 3
                drawer.close()
                }
            }

            Button {
                text: "ЛР5. Запросы по REST API."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 4
                drawer.close()
                }
            }

            Button {
                text: "ЛР6. Шифрование данных."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 5
                drawer.close()
                }
            }

            Button {
                text: "Граффические эффекты."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 6
                drawer.close()
                }
            }

            Button {
                text: "ЛР7. Чат-сервер."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 7
                drawer.close()
                }
             }

            Button {
                text: "ЛР8. Авторизация по JWT."
                font.pixelSize: 18
                flat: true
                onClicked: {
                swipeView.currentIndex = 8
                drawer.close()
                }
            }
        }
    }
}
