#ifndef VKMODEL_H
#define VKMODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include <QAbstractListModel>
#include <QList>
#include <QModelIndex>
#include <QVariant>
#include <QUrl>
#include <QVariant>

class vkObject
{
public:
    vkObject(
    const QUrl &photo,
    const QString &name,
    const QString &surname,
    const QString &city,
    const QString &domain);

    QUrl getPhoto() const;
    QString getName() const;
    QString getSurname() const;
    QString getCity() const;
    QString getDomain() const;

private:
    QUrl m_photo;
    QString m_name;
    QString m_surname;
    QString m_city;
    QString m_domain;
};

class vkModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum enmRoles {
        Photo,
        Name,
        Surname,
        City,
        Domain
    };

    vkModel(QObject *parent = nullptr);
    void addingNewItem(const vkObject & newItem);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant informationData(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;
    void clear();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<vkObject> m_items;
};

#endif // VKMODEL_H
