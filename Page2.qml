import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtMultimedia 5.15 //MediaPlayer
import QtQuick.Dialogs 1.0

Page {
    id:page2
    header: Rectangle {
        id:page2_header
        color: "#76b947"
        height: 27

        Label {
            text: qsTr("ЛР2. Работа с фото и видео.")
            font.pixelSize : 18
            anchors.topMargin: 3
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            height: 25
            width: 25
            anchors.right: page2_header.right
            anchors.bottomMargin: 1
            anchors.rightMargin: 10
         }

         Image {
            id: edit_image
            source: "images/edit.png"
            height: 25
            width: 25
            anchors.right: settings_image.left
            anchors.bottomMargin: 1
            anchors.rightMargin: 10
         }

         Image {
            id: spotify
            source: "images/spotify.png"
            height: 25
            width: 25
            anchors.left: page2_header.left
            anchors.bottomMargin: 1
            anchors.leftMargin: 5
         }
    }

    ColumnLayout{
            anchors.fill: parent

            RowLayout {
                id: choose_buttons
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.bottomMargin: 5
                Layout.alignment: Qt.AlignCenter //выравнивание по центру

                RadioButton {
                    id: video_check
                    checked: true
                    text: "Видео"
                    Universal.accent: "#76b947"
                }

                RadioButton {
                    id: photocam
                    text: "Фото"
                    Universal.accent: "#76b947"
                }

                Label {
                    text: "Выбрать..."
                }

                RoundButton {
                    id: upload_btn
                    flat: true
                    anchors.bottom: main_page2.top
                    anchors.margins: 2
                    Image{
                        source: "images/lab2_upload.png"
                        anchors.fill: upload_btn
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    onClicked: fileDialog.open()

                    FileDialog {
                        id: fileDialog
                        //selectFolder: true
                        folder : shortcuts.home
                        title: "Выбрать видео"
                        nameFilters: [qsTr("MP4 files (*.mp4)"), qsTr("All files (*.*)")]
                        onAccepted: running_video.source = fileDialog.fileUrl
                    }
                }
            }

            ColumnLayout{
                id: main_page2

                Rectangle {
                    id: video_page
                    anchors.bottomMargin: 30
                    anchors.rightMargin: 10
                    anchors.leftMargin: 10
                    anchors.fill: main_page2
                    color: "#76b947"

                    Item {
                        id: video_output
                        height: video_page.height
                        width: video_page.width

                        //Исчезновение панельки со слайдерами и паузой
                        Timer {
                            id: video_timer
                            interval: 5000
                            running: true
                            repeat: true
                            onTriggered: video_bottom.visible = false
                        }

                        MediaPlayer {
                            id: running_video
                            source: "video/legend.mp4"
                            volume: volumeSlider.volume
                            loops: MediaPlayer.Infinite // бесконечное повторение
                        }

                        VideoOutput {
                            anchors.fill: parent
                            source: running_video
                            flushMode: VideoOutput.FirstFrame
                        }

                        //Пауза по нажатию на видео
                        MouseArea {
                            id: tap_on
                            anchors.fill: parent
                            onPressed: {
                                running_video.playbackState == MediaPlayer.PlayingState ? running_video.pause() : running_video.play()
                                video_bottom.visible = true
                                video_timer.restart()
                            }
                        }

                        visible: {
                            if(video_check.checked == true) {
                                video_bottom.visible = true
                                return true
                            }
                            else {
                                running_video.pause()
                                video_bottom.visible = false
                            }
                        }
                    }

                   Item {
                        id: photo_page
                        anchors.fill: parent
                        anchors.verticalCenter: parent.verticalCenter
                        Camera {
                            id: camera_recorder
                        }
                        VideoOutput {
                            source: camera_recorder
                            anchors.fill: parent
                            autoOrientation: true
                        }
                        visible: {
                            if(photocam.checked == true) {
                                return true
                            }
                            else {
                                return false
                            }
                        }
                    }
                }
            }

             ColumnLayout {
                id: video_bottom
                RowLayout {
                    Button {
                        id: start_stop
                        anchors.leftMargin: 10
                        anchors.left:parent.left
                        icon.source: {
                            if (MediaPlayer.PlayingState == running_video.playbackState) {
                                return "images/pause.png"
                            }
                            else {
                                return "images/play.png"
                            }
                        }
                        onClicked: {
                            if (running_video.playbackState == MediaPlayer.PlayingState) {
                                return running_video.pause();
                            }
                            else {
                                return running_video.play();
                            }
                    }
                }
                    Slider {
                        id: video_slider
                        Universal.accent: "#76b947"

                        to: running_video.duration //продолжительность видео
                        value: running_video.position

                        Layout.fillWidth: true

                        onPressedChanged: {
                        running_video.seek(video_slider.value)
                        }
                    }
                    Text {
                        id: timer_video_num
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        font.pointSize: 10
                        color: "#76b947"

                        text: Qt.formatTime(new Date(running_video.position), "mm:ss")
                    }
                }
                RowLayout {
                    Layout.alignment: Qt.AlignCenter
                    anchors.bottomMargin: 20
                    Slider {
                        id: volumeSlider
                        Universal.accent: "#76b947"
                        from: 0
                        to: 1
                        value: 0.3

                        Image {
                            width: 30
                            height: 30
                            anchors.right: volumeSlider.left
                            source: {
                                if (volumeSlider.volume != 0) {
                                    return "images/turn_on.png"
                                }
                                else {
                                    return "images/turn_off.png";
                                }
                            }
                        }
                        Layout.maximumWidth: 150
                        Layout.minimumWidth: 70
                        Layout.fillWidth: true

                        property real volume: QtMultimedia.convertVolume(volumeSlider.value,
                        QtMultimedia.LogarithmicVolumeScale, QtMultimedia.LinearVolumeScale)
                    }
                }
                visible: {
                  running_video.pause()
                  if(video_check.checked == true) {
                     return true
                  }
                  else {
                      return false
                  }
                }
             }

            ColumnLayout{
                Layout.alignment: Qt.AlignCenter
                RowLayout {
                    Button{
                        id: take_photo
                        icon.source: "images/cam.png"
                        //onClicked: camera_recorder.imageCapture.captureToLocation("C:/Users/1353210/Documents/191_331_Mednikova/photo")
                        onClicked: camera_recorder.imageCapture.capture();
                    }
                    Button{
                        id: start_stop_record_video
                        icon.source: {
                            if (camera_recorder.videoRecorder.recorderStatus == CameraRecorder.RecordingStatus)
                                return "images/pause.png"
                            else
                                return "images/play.png"
                        }
                        onClicked: {
                            if (camera_recorder.videoRecorder.recorderState == CameraRecorder.StoppedState){
                                camera_recorder.videoRecorder.outputLocation = "C:\\Users\\1353210\\Documents\\191_331_Mednikova\\video_cam";
                                camera_recorder.videoRecorder.record() // начать запись
                            }
                            else {
                                camera_recorder.videoRecorder.stop() // остановить
                            }
                        }
                    }
                }

                visible: {
                    if(photocam.checked == true)
                        return true
                    else
                        return false
                  }
            }
      }
}
