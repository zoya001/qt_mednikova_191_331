import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtMultimedia 5.15
import QtQuick.Dialogs 1.0

Page {
    id: page9
    header: Rectangle {
        id:page1_header
        color: "#76b947"
        height: 32

        Connections {
            target: httpController // Указываем объект для соединения
            onTokenToQml: {
                tokenShow.text = token
            }
            onDataToQml: {
                base64data.text = pageContent;
            }
        }

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page1_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page1_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            text: qsTr("ЛР8. Авторизация по JWT.")
            font.pixelSize : 25
            anchors.verticalCenter: page1_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page1_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page1_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page1_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    ColumnLayout {
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
         anchors.margins: 20
         id: mainLayout
         anchors.fill: parent
             RowLayout {
                 Label {
                     id: loginLabel
                     font.pixelSize: 18
                     text: "E-mail"
                 }
                     TextField {
                         id: login
                         Layout.fillWidth: true
                         Universal.accent: "#76b947"
                     }
                 }

             RowLayout {
                 Label {
                     id: passwordLabel
                     font.pixelSize: 18
                     text: "Пароль"
                 }
                 TextField {
                     id: password
                     Layout.fillWidth: true
                     Universal.accent: "#76b947"
                 }
             }

             RowLayout {
                 Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                 Layout.fillWidth: true
                 Button {
                     id: send
                     text: "Получить"
                     background: Rectangle {
                         color: "#76b947"
                         radius: 4
                     }
                     font.pixelSize: 18
                     onClicked: {
                         httpController.logIN(login.text, password.text);
                         news.visible = true;
                     }
                 }
            }

             RowLayout {
                 TextArea {
                     id: tokenShow
                     readOnly: true
                     anchors.fill: parent
                     Layout.fillWidth: true
                     Universal.accent: "#76b947"
                 }
         }

        RowLayout {
             ScrollView {
                 clip:  true
                 anchors.top: tokenShow.bottom
                 anchors.left: parent.left
                 anchors.right: parent.right
                 anchors.bottom: parent.bottom
                 anchors.margins: 20
                 Layout.fillWidth: true
                 TextArea {
                     id: base64data
                     anchors.fill: parent
                     textFormat: Text.RichText
                     readOnly: true
                     color: "black"
                     background: Rectangle {
                         id: news
                         color: "#76b947"
                         visible: false
                     }
                 }
             }
        }
    }
}
