import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1

Page{
    id: page4
    header: Rectangle {
        id:page4_header
        color: "#76b947"
        height: 32

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page4_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page4_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            text: qsTr("ЛР4. Аутентификация OAuth2.")
            font.pixelSize : 18
            anchors.verticalCenter: page4_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page4_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page4_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page4_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    ColumnLayout {
        id: column
        anchors.fill: parent
        anchors.bottomMargin: 20
        WebView {
            id: webView
            Layout.fillWidth: true
            Layout.fillHeight: true
            url:"https://oauth.vk.com/authorize?client_id=7851317&redirect_uri=&response_type=token&scope=0&v=5.92"
            onLoadingChanged: {
                var token = httpController.showToken(webView.url)

                getPhoto()

                if(token !== " ") {
                    tokens.text = token //получаем токен
                    tokens.visible = true
                    req_btn.visible = true
                    webView.visible = false
                }
            }
        }

        Label {
            id:tokens //записываем полученный токен как текст
            visible: false
            font.pixelSize: 15
            Layout.alignment: Qt.AlignCenter
            objectName: "Token"
        }
    }

        RowLayout {
            anchors.verticalCenter: page4_header.horizontalCenter
            Button {
                id: req_btn
                anchors.leftMargin: 20
                anchors.rightMargin: 20
                font.pixelSize : 20
                text: "Вывод списка"
                onClicked: {
                    restRequest(); //получение списка друзей для 5 лр
                }
             visible: false
             Layout.fillWidth: true
            }
    }
}
