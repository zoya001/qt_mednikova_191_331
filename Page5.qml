import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtGraphicalEffects 1.0

Page {
    id: page5
    header: Rectangle {
        id:page5_header
        color: "#76b947"
        height: 32

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page5_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page5_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            text: qsTr("ЛР5. Запросы по REST API.")
            font.pixelSize : 18
            anchors.verticalCenter: page5_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page5_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page5_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page5_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    ColumnLayout{
        anchors.fill: parent
        Layout.alignment: Qt.AlignHCenter

        RowLayout{
            Layout.alignment: Qt.AlignHCenter

            RadioButton {
                id: columnBtn
                text: qsTr("Столбцы")
                checked: true
                Universal.accent: "#76b947"
                onCheckedChanged:
                    if(columnBtn.checked) {
                        grid.cellWidth = grid.width/(Math.round(grid.width/300))
                    }
                    else {
                        grid.cellWidth = grid.width
                    }
            }
            RadioButton {
               id: rowBtn
               text: qsTr("Строки")
               Universal.accent: "#76b947"
            }
        }

        GridView {
            onWidthChanged: {
                if (columnBtn.checked) {
                    grid.cellWidth = grid.width/(Math.round(grid.width/300))
                }
                else {
                    grid.cellWidth = grid.width
                }
            }
            clip: true
            id: grid
            cellWidth: width/(Math.round(width/300))
            cellHeight: 140
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.leftMargin: 5
            Layout.rightMargin: 5
            model: vk_model
            delegate: Rectangle {
                color: "#76b947"
                width: grid.cellWidth
                height: grid.cellHeight
                border.color: "#001624"
                GridLayout {
                    anchors.fill: parent
                    columns: 3
                    rows: 4
                    Image{
                        id: img
                        source: pic
                        Layout.column: 0
                        Layout.row: 0
                        Layout.rowSpan: 3
                        Layout.preferredHeight: 90
                        Layout.preferredWidth: 90
                        Layout.margins: 5
                        fillMode: Image.PreserveAspectFit
                    }

                    Label {
                        text: "ID: " + domain
                        Layout.topMargin: 10
                        Layout.column: 2
                        Layout.row: 0
                        Layout.fillHeight: true
                        Layout.preferredWidth: 150
                    }

                    Label {
                        text: "Имя: " + name
                        Layout.column: 2
                        Layout.row: 1
                        Layout.fillHeight: true
                        Layout.preferredWidth: 150
                    }

                    Label {
                        text: "Фамилия: " + surname
                        Layout.column: 2
                        Layout.row: 2
                        Layout.fillHeight: true
                        Layout.preferredWidth: 150
                    }

                    Label {
                        text: "Город: " + city
                        Layout.column: 2
                        Layout.row: 3
                        Layout.fillHeight: true
                        Layout.preferredWidth: 150
                    }
                }
            }
        }
    }
}
