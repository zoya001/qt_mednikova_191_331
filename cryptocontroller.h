#ifndef ENCRYPTION_CONTROLLER_H
#define ENCRYPTION_CONTROLLER_H
#include <QString>
#include <QObject>

class Encryptioncontroller:public QObject
{
    Q_OBJECT
public:
    explicit Encryptioncontroller(QObject *parent = nullptr);
    QString soursefile;

    signals:
    void sendChiprToQml(QString codetext);

public slots:
    bool encryptFile(const QString & mkey, const QString &in_file); //передаем ключ и файл
    bool decryptFile(const QString & mkey, const QString &in_file);

private:
    unsigned char * key = (unsigned char *)("3210423012345889873884547892764"); //дефолтный ключ
                                                                                 //32 символа
    QString key2;

protected:
    QObject *EncryptFile;
};

#endif // ENCRYPTION_CONTROLLER_H
