#include "vkmodel.h"
#include <QAbstractItemModel>
#include <QMap>

vkObject::vkObject( const QUrl &getPhoto, const QString &getName, const QString &getSurname, const QString &getCity, const QString &getDomain)
    :
        m_photo(getPhoto),
        m_name(getName),
        m_surname(getSurname),
        m_city(getCity),
        m_domain(getDomain)
{
}

vkModel::vkModel(QObject *parent) : QAbstractListModel(parent){}

QUrl vkObject::getPhoto() const {
    return m_photo;
}

QString vkObject::getName() const {
    return m_name;
}

QString vkObject::getSurname() const {
    return m_surname;
}

QString vkObject::getCity() const {
    return m_city;
}

QString vkObject::getDomain() const {
    return m_domain;
}

//записываюь сюда массив строк, который мы напарсили
void vkModel::addingNewItem(const vkObject & newItem){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_items << newItem;
    endInsertRows(); //встроенный
}

//считываем количество элементов массива
//в который мы записываем после парсинга джсон
int vkModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_items.count();
}

//перебираем айтемсы и записываем их в роли
QVariant vkModel::informationData(const QModelIndex & index, int role) const
{
    if (index.row() < 0 || (index.row() >= m_items.count()))
        //перебираем m_item записываем из в роли
        return QVariant();

    const vkObject&itemToReturn = m_items[index.row()];
     if (role == Photo)
    return itemToReturn.getPhoto();
    else if (role == Name)
    return itemToReturn.getName();
    else if (role == Surname)
    return itemToReturn.getSurname();
    else if (role == City)
    return itemToReturn.getCity();
    else if (role == Domain)
    return itemToReturn.getDomain();

    return QVariant();
}

//из ролей записываем в объекты
QHash<int, QByteArray> vkModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[Photo] = "pic";
    roles[Name] = "name";
    roles[Surname] = "surname";
    roles[City] = "city";
    roles[Domain] = "domain";
    return roles;
}

QVariantMap vkModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = informationData(index(idx, 0), k);
    }
    return map;
}

void vkModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);
    m_items.clear();
    endRemoveRows();
}
