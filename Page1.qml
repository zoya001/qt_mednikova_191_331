import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Styles 1.4
import QtQml 2.15

//https://newatlas.com/spotify-free-tier-upgrade/54351/
//ЛР1. Введение. Элементы управления

Page {
    id:page1
    header: Rectangle {
        id:page1_header
        color: "#76b947"
        height: 32

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page1_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page1_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            text: qsTr("ЛР1. Элементы GUI.")
            font.pixelSize : 25
            anchors.verticalCenter: page1_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page1_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page1_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page1_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    Image {
        Layout.alignment: Qt.AlignCenter
        source: "images/lab1.jpg"
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left

        GridLayout {
              anchors.fill:parent //растянуть по родительскому элементу
              anchors.margins: 10

              ColumnLayout {
                Label {
                     anchors.topMargin: 10
                     id: choice
                     text: "Вы ознакомлены с правилами?"
                     font.pixelSize: 20
                     font.family:"Helvetica"
                 }

              ColumnLayout {
                RadioButton {
                    id: right_choice
                    text: qsTr("Да")
                    font.pixelSize : 18
                    font.family:"Helvetica"
                    Layout.fillHeight: true
                    checked: true
                    indicator: Rectangle {
                        implicitWidth: 26
                        implicitHeight: 26
                        x: right_choice.leftPadding
                        y: parent.height / 2 - height / 2
                        radius: 13
                        border.color: right_choice.down ? "#007046" : "#76b947"
                        Rectangle {
                            width: 14
                            height: 14
                            x: 6
                            y: 6
                            radius: 7
                            color: right_choice.down ? "#007046" : "#76b947"
                            visible: right_choice.checked
                        }
                    }
                    contentItem: Text {
                        text: right_choice.text
                        font: right_choice.font
                        opacity: enabled ? 1.0 : 0.3
                        color: right_choice.down ? "#007046" : "white"
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: right_choice.indicator.width + right_choice.spacing
                    }
                    onClicked: {
                        wrong_choice.checked = false
                    }
                }
              }

              ColumnLayout {
                RadioButton {
                    id: wrong_choice
                    text: qsTr("Нет")
                    font.pixelSize : 18
                    font.family:"Helvetica"
                    Layout.fillHeight: true
                    indicator: Rectangle {
                        implicitWidth: 26
                        implicitHeight: 26
                        x: wrong_choice.leftPadding
                        y: parent.height / 2 - height / 2
                        radius: 13
                        border.color: wrong_choice.down ? "#007046" : "#76b947"
                        Rectangle {
                            width: 14
                            height: 14
                            x: 6
                            y: 6
                            radius: 7
                            color: wrong_choice.down ? "#007046" : "#76b947"
                            visible: wrong_choice.checked
                        }
                    }
                    contentItem: Text {
                        text: wrong_choice.text
                        font: wrong_choice.font
                        opacity: enabled ? 1.0 : 0.3
                        color: wrong_choice.down ? "#007046" : "white"
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: wrong_choice.indicator.width + wrong_choice.spacing
                    }
                    onClicked: {
                        right_choice.checked = false
                    }
                }
             }

              ColumnLayout {
                  RowLayout {
                     Switch {
                         id: switch_page1
                         text: "Включить звук"
                         font.pixelSize : 18
                         font.family:"Helvetica"

                         indicator: Rectangle {
                             implicitWidth: 40
                             implicitHeight: 20
                             x: switch_page1.leftPadding
                             y: parent.height / 2 - height / 2
                             radius: 13
                             color: switch_page1.checked ? "#76b947" : "#ffffff"
                             border.color: switch_page1.checked ? "#76b947" : "#cccccc"

                             Rectangle {
                                 x: switch_page1.checked ? parent.width - width : 0
                                 width: 20
                                 height: 20
                                 radius: 10
                                 color: switch_page1.down ? "#cccccc" : "#ffffff"
                                 border.color: switch_page1.checked ? (switch_page1.down ? "#76b947" : "#007046") : "#999999"
                             }
                         }

                         contentItem: Text {
                             text: switch_page1.text
                             font: switch_page1.font
                             opacity: enabled ? 1.0 : 0.3
                             color: switch_page1.down ? "#76b947" : "white"
                             verticalAlignment: Text.AlignVCenter
                             leftPadding: switch_page1.indicator.width + switch_page1.spacing
                         }
                     }
                  }
              }

              ColumnLayout {
                  RowLayout {
                         Label {
                            id: delay_text
                            font.pixelSize : 18
                            anchors.topMargin: 20
                            text: "Зажмите для подтверждения действия:"
                         }

                     DelayButton {
                            id:delaybutton
                            delay: 1000
                            onActivated: {
                                delaybutton.progress = 0.0
                            }
//                            DelayButtonStyle {
//                                progressBarDropShadowColor : "#007046"
//                            }
                            Universal.accent: "#76b947"
                        }
                   }
              }

              ColumnLayout {
                      RowLayout {
                          Dial {
                              id: dial_page1
                              background: Rectangle {
                                  x: dial_page1.width / 2 - width / 2
                                  y: dial_page1.height / 2 - height / 2
                                  width: Math.max(64, Math.min(dial_page1.width, dial_page1.height))
                                  height: width
                                  color: "transparent"
                                  radius: width / 2
                                  border.color: dial_page1.pressed ? "#007046" : "#76b947"
                                  opacity: dial_page1.enabled ? 1 : 0.3
                              }

                              handle: Rectangle {
                                  id: handleItem
                                  x: dial_page1.background.x + dial_page1.background.width / 2 - width / 2
                                  y: dial_page1.background.y + dial_page1.background.height / 2 - height / 2
                                  width: 10
                                  height: 10
                                  color: dial_page1.pressed ? "#76b947" : "#007046"
                                  radius: width / 2
                                  antialiasing: true
                                  opacity: dial_page1.enabled ? 1 : 0.3
                                  transform: [
                                      Translate {
                                          y: -Math.min(dial_page1.background.width, dial_page1.background.height) * 0.4 + handleItem.height / 2
                                      },
                                      Rotation {
                                          angle: dial_page1.angle
                                          origin.x: handleItem.width / 2
                                          origin.y: handleItem.height / 2
                                      }
                                  ]
                              }
                              Layout.fillHeight: true
                              Layout.fillWidth: true
                         }

                         RangeSlider {
                             id: slider_page1
                             first.value: 0.25
                             second.value: 0.75
                             Layout.fillHeight: true
                             Layout.fillWidth: true
                             background: Rectangle {
                                 x: slider_page1.leftPadding
                                 y: slider_page1.topPadding + slider_page1.availableHeight / 2 - height / 2
                                 implicitWidth: 200
                                 implicitHeight: 4
                                 width: slider_page1.availableWidth
                                 height: implicitHeight
                                 radius: 2
                                 color: "#007046"

                                 Rectangle {
                                     x: slider_page1.first.visualPosition * parent.width
                                     width: slider_page1.second.visualPosition * parent.width - x
                                     height: parent.height
                                     color: "#76b947"
                                     radius: 2
                                 }
                             }

                             first.handle: Rectangle {
                                 x: slider_page1.leftPadding + slider_page1.first.visualPosition * (slider_page1.availableWidth - width)
                                 y: slider_page1.topPadding + slider_page1.availableHeight / 2 - height / 2
                                 implicitWidth: 26
                                 implicitHeight: 26
                                 radius: 13
                                 color: slider_page1.first.pressed ? "#73AE2C" : "white"
                                 border.color: "#007046"
                             }

                             second.handle: Rectangle {
                                 x: slider_page1.leftPadding + slider_page1.second.visualPosition * (slider_page1.availableWidth - width)
                                 y: slider_page1.topPadding + slider_page1.availableHeight / 2 - height / 2
                                 implicitWidth: 26
                                 implicitHeight: 26
                                 radius: 13
                                 color: slider_page1.second.pressed ? "#73AE2C" : "white"
                                 border.color: "#007046"
                             }
                         }
                      }
                  }

              ColumnLayout {
                     id: row_page1
                     anchors.topMargin: 25
                     anchors.leftMargin: 25
                     Layout.row: 6
                     Layout.columnSpan: 2
                     RowLayout {
                         Tumbler {
                             id: hours_tumbler
                             model: 12
                             font.pixelSize : 15
                             font.family:"Helvetica"
                             Layout.fillHeight: true
                             Layout.fillWidth: true
                             Rectangle {
                                 anchors.horizontalCenter: hours_tumbler.horizontalCenter
                                 y: hours_tumbler.height * 0.4
                                 width: 40
                                 height: 1
                                 color: "#76b947"
                             }
                             Rectangle {
                                 anchors.horizontalCenter: hours_tumbler.horizontalCenter
                                 y: hours_tumbler.height * 0.6
                                 width: 40
                                 height: 1
                                 color: "#76b947"
                             }
                         }

                         Tumbler {
                             id: minutes_tumbler
                             model: 60
                             font.pixelSize : 15
                             font.family:"Helvetica"
                             Layout.fillHeight: true
                             Layout.fillWidth: true
                             Rectangle {
                                 anchors.horizontalCenter: minutes_tumbler.horizontalCenter
                                 y: minutes_tumbler.height * 0.4
                                 width: 40
                                 height: 1
                                 color: "#76b947"
                             }
                             Rectangle {
                                 anchors.horizontalCenter: minutes_tumbler.horizontalCenter
                                 y: minutes_tumbler.height * 0.6
                                 width: 40
                                 height: 1
                                 color: "#76b947"
                             }
                         }

                         Tumbler {
                             id: am_pm_tumbler
                             model: ["Утра", "Ночи"]
                             font.pixelSize : 15
                             font.family:"Helvetica"
                             Layout.fillHeight: true
                             Layout.fillWidth: true
                             Rectangle {
                                 anchors.horizontalCenter: am_pm_tumbler.horizontalCenter
                                 y: am_pm_tumbler.height * 0.4
                                 width: 40
                                 height: 1
                                 color: "#76b947"
                             }
                             Rectangle {
                                 anchors.horizontalCenter: am_pm_tumbler.horizontalCenter
                                 y: am_pm_tumbler.height * 0.6
                                 width: 40
                                 height: 1
                                 color: "#76b947"
                             }
                         }
                    }
                }
            }
        }
    }
}
