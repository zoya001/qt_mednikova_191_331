import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtGraphicalEffects 1.15
import QtQuick.Dialogs 1.3

Page {
    id: page7
    header: Rectangle {
        id:page7_header
        color: "#76b947"
        height: 32

        Image {
           id: spotify
           source: "images/spotify.png"
           anchors.left: page7_header.left
           height: 28
           width: 28
           anchors.verticalCenter: page7_header.verticalCenter
           anchors.leftMargin: 10
        }

        Label {
            text: qsTr("Граффические эффекты.")
            font.pixelSize : 18
            anchors.verticalCenter: page7_header.verticalCenter
            anchors.left: spotify.right
            anchors.leftMargin: 5
        }

        Image {
           id: edit_image
           source: "images/edit.png"
           anchors.right: settings_image.left
           height: 28
           width: 28
           anchors.verticalCenter: page7_header.verticalCenter
           anchors.rightMargin: 10
        }

         Image {
            id: settings_image
            source: "images/settings.png"
            anchors.right: page7_header.right
            height: 28
            width: 28
            anchors.verticalCenter: page7_header.verticalCenter
            anchors.rightMargin: 10
         }
    }

    //FastBlur, RadialBlur, RadialGradient
    //Вариант 16

    ColumnLayout {
        id: effects_page_body
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.fill: parent
        anchors.margins: 10
        TabBar {
            id: effects_pages
            width: parent.width
            anchors.top: parent.top
            anchors.fill: effects_page_body
            Universal.accent: "#76b947"
            TabButton {
                text: qsTr("FastBlur")
                onClicked: {
                    fastblur.visible = true
                    image_body1.visible = true
                    radialblur.visible = false
                    image_body2.visible = false
                    radialgradient.visible = false
                    image_body3.visible = false
                }
                Universal.accent: "#76b947"
            }

            TabButton {
                text: qsTr("RadialBlur")
                onClicked: {
                    fastblur.visible = false
                    image_body1.visible = false
                    radialblur.visible = true
                    image_body2.visible = true
                    radialgradient.visible = false
                    image_body3.visible = false
                }
                Universal.accent: "#76b947"
            }

            TabButton {
                text: qsTr("RadialGradient")
                onClicked: {
                    fastblur.visible = false
                    image_body1.visible = false
                    radialblur.visible = false
                    image_body2.visible = false
                    radialgradient.visible = true
                    image_body3.visible = true
                }
                Universal.accent: "#76b947"
            }
        }

        ColumnLayout {
            width: effects_pages.width - 20
            anchors.horizontalCenter: effects_pages.horizontalCenter
            ColumnLayout {
                id:image_body1
                anchors.top: effects_pages.bottom
                anchors.horizontalCenter: effects_page_body.horizontalCenter
                visible: true
                Image {
                    id: picture1
                    source: "images/picture.png"
                    width: image_body1.width
                    height: image_body1.height
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    visible: false
                }
                FastBlur {
                    id: fastblur_effect
                    anchors.fill: picture1
                    source: picture1
                    radius: radius_slider.value
                    transparentBorder: transparentBorder_button.checked
                }
            }

            ColumnLayout {
                id:image_body2
                anchors.top: effects_pages.bottom
                anchors.horizontalCenter: effects_page_body.horizontalCenter
                visible: false
                Image {
                    id: picture2
                    source: "images/picture.png"
                    width: image_body2.width
                    height: image_body2.height
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    visible: false
                }
                RadialBlur {
                    id: radialblur_effect
                    anchors.fill: picture2
                    source: picture2
                    angle: radialblur_slider.value
                    horizontalOffset: radialblur_slider1.value
                    verticalOffset: radialblur_slider2.value
                    transparentBorder: transparentBorder_button2.checked
                    samples: 24
                }
            }

            ColumnLayout {
                id:image_body3
                anchors.top: effects_pages.bottom
                anchors.horizontalCenter: effects_page_body.horizontalCenter
                visible: false
                Image {
                    id: picture3
                    source: "images/picture1.png"
                    width: image_body3.width
                    height: image_body3.height
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    visible: false
                }
                RadialGradient {
                    id: radialgradient_effect
                    anchors.fill: picture3
                    source: picture3
                    angle: radialgradient_slider.value
                    horizontalOffset: radialgradient_slider1.value
                    horizontalRadius: radialgradient_slider2.value
                    verticalRadius: radialgradient_slider3.value
                }
            }

            RowLayout {
                anchors.top: image_body.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                ColumnLayout {
                    id: fastblur
                    visible: true
                    RowLayout {
                        Layout.fillWidth: true
                        Text {
                            color: "#76b947"
                            text: qsTr("radius")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radius_slider
                            Layout.fillWidth: true
                            from: 0
                            to: 64
                            stepSize: 2
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radius_slider.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        anchors.horizontalCenter: parent.horizontalCenter
                        Text {
                            color: "#76b947"
                            text: qsTr("transparent border")
                            font.pixelSize : 18
                        }
                        Switch {
                            id: transparentBorder_button
                            checked: false
                            Universal.accent: "#76b947"
                        }
                    }
                    RowLayout {
                        anchors.horizontalCenter: parent.horizontalCenter
                        Button {
                            id: dialog_btn1
                            text: "Сохранить изображение"
                            onClicked: saveFileDialog1.open()
                            font.pixelSize : 18
                            FileDialog {
                                id: saveFileDialog1
                                title: "Save file"
                                nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]
                                selectExisting: false
                                onAccepted:
                                {
                                    var url = (saveFileDialog1.fileUrl.toString()+"").replace('file:///', '');
                                    image_body1.grabToImage(function(result) {
                                                               result.saveToFile(url);
                                                           });
                                }
                            }
                        }
                    }
                }
                    ColumnLayout {
                        id: radialblur
                        visible: false
                        anchors.horizontalCenter: parent.horizontalCenter
                        RowLayout {
                        Text {
                            color: "#76b947"
                            text: qsTr("angle")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radialblur_slider
                            Layout.fillWidth: true
                            from: 0
                            to: 30
                            stepSize: 2
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radialblur_slider.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }
                    RowLayout {
                        Text {
                            color: "#76b947"
                            text: qsTr("horizontal offset")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radialblur_slider1
                            Layout.fillWidth: true
                            from: 75.0
                            to: -75.0
                            stepSize: 1.0
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radialblur_slider1.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }
                    RowLayout {
                        Text {
                            color: "#76b947"
                            text: qsTr("vertical offset")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radialblur_slider2
                            Layout.fillWidth: true
                            from: 75.0
                            to: -75.0
                            stepSize: 1.0
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radialblur_slider2.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }
                    RowLayout {
                        anchors.horizontalCenter: parent.horizontalCenter
                        Button {
                            id: dialog_btn2
                            text: "Сохранить изображение"
                            onClicked: saveFileDialog2.open()
                            font.pixelSize : 18
                            FileDialog {
                                id: saveFileDialog2
                                title: "Save file"
                                nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]
                                selectExisting: false
                                onAccepted:
                                {
                                    var url = (saveFileDialog2.fileUrl.toString()+"").replace('file:///', '');
                                    image_body2.grabToImage(function(result) {
                                                               result.saveToFile(url);
                                                           });
                                }
                            }
                        }
                    }
                }
                    ColumnLayout {
                        id: radialgradient
                        visible: false
                        anchors.horizontalCenter: parent.horizontalCenter
                        RowLayout {
                        Text {
                            color: "#76b947"
                            text: qsTr("angle")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radialgradient_slider
                            Layout.fillWidth: true
                            from: 0
                            to: 90
                            stepSize: 5
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radialgradient_slider.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }
                    RowLayout {
                        Text {
                            color: "#76b947"
                            text: qsTr("horizontal offset")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radialgradient_slider1
                            Layout.fillWidth: true
                            from: -150.0
                            to: 150.0
                            stepSize: 5.0
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radialgradient_slider1.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }

                    RowLayout {
                        Text {
                            color: "#76b947"
                            text: qsTr("horizontal radius")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radialgradient_slider2
                            Layout.fillWidth: true
                            from: 0
                            to: 300
                            stepSize: 2
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radialgradient_slider2.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }

                    RowLayout {
                        Text {
                            color: "#76b947"
                            text: qsTr("vertical radius")
                            font.pixelSize : 18
                        }
                        Slider {
                            id: radialgradient_slider3
                            Layout.fillWidth: true
                            from: 0
                            to: 300
                            stepSize: 2
                            Universal.accent: "#76b947"
                        }
                        Text {
                            text: qsTr(radialgradient_slider3.value.toFixed(2))
                            color: "#76b947"
                            font.pixelSize : 18
                        }
                    }

                    RowLayout {
                        anchors.horizontalCenter: parent.horizontalCenter
                        Button {
                            id: dialog_btn3
                            text: "Сохранить изображение"
                            onClicked: saveFileDialog3.open()
                            font.pixelSize : 18
                            FileDialog {
                                id: saveFileDialog3
                                title: "Save file"
                                nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]
                                selectExisting: false
                                onAccepted:
                                {
                                    var url = (saveFileDialog3.fileUrl.toString()+"").replace('file:///', '');
                                    image_body3.grabToImage(function(result) {
                                                               result.saveToFile(url);
                                                           });
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
