#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QNetworkReply>
#include "webappcontroller.h"
#include "vkmodel.h"
#include "cryptocontroller.h"

//ДЛЯ 9 ЛАБЫ *************************

//#if defined(ANDROID)
//    #include <QAndroidJniObject>
//    #include <QAndroidJniEnvironment>
//    #include <QtAndroid>
//#endif  // defined(ANDROID)

//#include "firebase/analytics.h"
//#include "firebase/analytics/event_names.h"
//#include "firebase/analytics/parameter_names.h"
//#include "firebase/analytics/user_property_names.h"
//#include <firebase/app.h>

//namespace analytics = ::firebase::analytics;

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine; //создание объекта движка-интерпретатора QML

    const QUrl url(QStringLiteral("qrc:/main.qml")); //задание файла QML-разметки для старотовой разметки окна приложения

    qDebug() << "*** URL = " << url;
    //конструкция ниже задает связь между событием "objectCreated" объекта "engine" и коллбеком
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app,
                    [url](QObject *obj, const QUrl &objUrl) //коллбек, лямбда-выражение, безымянная функция, объявленная единоразово
    {
        if (!obj && url == objUrl) QCoreApplication::exit(-1); //действие на случай ошибки внутри движка
    }, Qt::QueuedConnection);

    engine.load(url); //загрузить файл старотовой страницы в движок

    Encryptioncontroller crypt;

    QObject *rootItem = engine.rootObjects()[0];
    WebAppController httpController(rootItem);

    QObject::connect(rootItem, SIGNAL(btnHttpRequest()),
               &httpController, SLOT(getPageInfo())); // связь сигнала из qml при нажатии на кнопку и слота класса HTTPController

    QQmlContext *context = engine.rootContext();
    context->setContextProperty("vk_model", httpController.vk_model); //Перемещаемая модель, которой присваиваем имя
    context->setContextProperty("httpController", &httpController);
       //преобразование пути стартовой страницы из char в Qurl

    QObject::connect(engine.rootObjects().first(), SIGNAL(restRequest()),
               &httpController, SLOT(restRequest()));

    QObject::connect(engine.rootObjects().first(), SIGNAL(showToken(QString)),
               &httpController, SLOT(showToken(QString))); // вызов сигнала

    QObject* main = engine.rootObjects()[0];
    WebAppController sendtoqml(main);
    engine.rootContext()->setContextProperty("_send", &sendtoqml);
    context -> setContextProperty("crypt", &crypt); //связка через контекстное свойство

    QObject::connect(engine.rootObjects().first(), SIGNAL(getPhoto()),
                &httpController, SLOT(getPhoto()));

    //ДЛЯ 9 ЛАБЫ *************************

//    firebase::App * fb_app = nullptr; //инициализация приложения firebase

//   //Директивы препроцессора

//        #if defined(ANDROID)
//            QAndroidJniEnvironment qjniEnv;
//            QAndroidJniObject qactivity = QtAndroid::androidActivity();
//            fb_app = firebase::App::Create(firebase::AppOptions(), qjniEnv, qactivity.object());
//        #else
//            fb_app = firebase::App::Create(firebase::AppOptions());
//        #endif  // defined(ANDROID)

//        // инициализация подсистемы аналитики
//        firebase::analytics::Initialize(*fb_app);
//        firebase::analytics::SetAnalyticsCollectionEnabled(true); //сбор данных
//        auto result = app.exec();

//        firebase::analytics::Terminate(); //прекращение отслеживания
//        delete fb_app;
//        return result;

    return app.exec(); //НАЧАЛО работы приложения -  пеередача управления, от точки входа к коду самого приложения
}
